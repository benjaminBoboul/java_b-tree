import imt.btree.Tree;


// Corentin Le Berre et Benjamin Boboul
public class main {
    public static void main(String[] args) {
        Tree racine = new Tree();
        Tree.setOrder(3);
        for (int i = 0; i<10 ; i++) {
            int valeur = (int) (Math.random() * 100);
            racine.addEntry(valeur);
            racine.afficher();
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (valeur % 2 != 1) {
                System.out.println("" + valeur + " est présent ? " + racine.isPresent(valeur));
            }
        }

    }
}
